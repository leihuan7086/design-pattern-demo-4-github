package jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JdkProxy implements InvocationHandler {
    // 目标类，也就是被代理对象
    private Object target;

    public JdkProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 此处为增强功能
        System.out.println("extend function");

        // 调用target的method方法
        return method.invoke(target, args);
    }
}
