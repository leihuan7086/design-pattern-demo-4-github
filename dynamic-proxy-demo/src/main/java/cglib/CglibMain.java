package cglib;

import net.sf.cglib.proxy.Enhancer;

public class CglibMain {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CglibExample.class);
        enhancer.setCallback(new CglibProxy());
        Object object = enhancer.create();

        // proxyCglibExample继承CglibExample,类型"CglibExample$$EnhancerByCGLIB$$+字符串"
        CglibExample proxyCglibExample = (CglibExample) object;
        System.out.println("cglibExample: " + proxyCglibExample.getClass().getName());

        proxyCglibExample.methodOne();
    }

}
